## Репозитории

### [app](https://gitlab.com/klovtsov/deusops_lessons/230819/app)
pipeline dev [![pipeline status](https://gitlab.com/klovtsov/deusops_lessons/230819/app/badges/dev/pipeline.svg)](https://gitlab.com/klovtsov/deusops_lessons/230819/app/-/commits/dev)

pipeline prod [![pipeline status](https://gitlab.com/klovtsov/deusops_lessons/230819/app/badges/main/pipeline.svg)](https://gitlab.com/klovtsov/deusops_lessons/230819/app/-/commits/main)

[gitlab-ci.yml@klovtsov/devops_includes/gitlab_ci_flows/230819:v1.0.1](https://gitlab.com/klovtsov/devops_includes/gitlab_ci_flows/230819/-/blob/v1.0.1/gitlab-ci.yml?ref_type=tags)

### [infra/tf](https://gitlab.com/klovtsov/deusops_lessons/230819/infra/tf)
Группа с репозиториями terraform кода по стендам

infra dev [![pipeline status](https://gitlab.com/klovtsov/deusops_lessons/230819/infra/tf/dev/badges/main/pipeline.svg)](https://gitlab.com/klovtsov/deusops_lessons/230819/infra/tf/dev/-/commits/main) 

infra prod [![pipeline status](https://gitlab.com/klovtsov/deusops_lessons/230819/infra/tf/prod/badges/main/pipeline.svg)](https://gitlab.com/klovtsov/deusops_lessons/230819/infra/tf/prod/-/commits/main)

[lint-plan-deploy-ansible.yml@klovtsov/devops_includes/gitlab_ci_flows/terraform-ansible:main](https://gitlab.com/klovtsov/devops_includes/gitlab_ci_flows/terraform-ansible/-/blob/main/lint-plan-deploy-ansible.yml?ref_type=heads)

### [infra/ansible](https://gitlab.com/klovtsov/deusops_lessons/230819/infra/ansible)
Репозиторий с настройками для ansible и ansible playbookами

### [infra/inventories](https://gitlab.com/klovtsov/deusops_lessons/230819/infra/inventories)
Группа с репозиториями ansible inventory по стендам

Для инвентаря используется [ansible inventory plugin](https://gitlab.com/klovtsov/devops_includes/ansible_collections/yc_inventory/-/blob/main/klovtsov/yc_inventory/plugins/inventory/yc_compute.py?ref_type=heads), который устанавливается из коллекции с помощью ansible-galaxy

## Gitlab CI/CD Variables
